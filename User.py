import json
import random       #import important libraries
import sys


class User:
    def __init__(self):
        self.user_url = "user.json"
        self.users = {}

    def get_user_data(self):
        """loads all user data from the user.json file into the self.users dict"""
        try:
            with open("user.json", "r+") as f:      #load all user data from json into program
                self.temp = json.load(f)
                for i, j in self.temp.items():
                    self.users[i] = j
        except:
            pass

    def save_data(self, dictionary):
        """saves/updates the user.json file, in case a new password was set or an account created"""
        with open("user.json", "w+") as f:
            json.dump(dictionary, f)

    def log_in(self):
        """login page, where the user can log in, resets his/her password or creates a user account"""
        active = True
        while active:
            username = input(
                "please enter your username\nif you forgot your password please type 'forgot\nif you want to create an account, please type 'ini\nprint 'quit' to exit the process\n>")

            if username == "forgot":
                self.forgot()
                continue

            if username == "ini":
                self.initialize()
                continue

            if username == 'quit':
                sys.exit("enjoy your day")

            if username in self.users.keys():
                password = input("please enter your password\n>")
                if self.users[username][0] == password: #checks if username and password are correct and returns username for active_user
                    active = False
                    return username
                else:
                    print("the password is not correct")
            else:
                print("the username is not correct")

    def forgot(self):
        """in case a user forgets his/her password, username and security key must be provided. Then, a new password will picked and saved under his/her name in the user.json file"""
        active = True
        while active:
            username = input("please enter your username\nprint 'quit' to exit the process\n>")
            if username in self.users.keys():       #if the username exist, then
                skey = input("please enter your security key\n>")       
                if skey == self.users[username][1]: #if the security key is correct
                    new_password = input("please enter your desired password\n>")
                    new_reenter = input("please enter again\n>")
                    if new_password == new_reenter: #new password can be entered, if they match, they will be saved
                        self.users[username][0] = new_password
                        active = False
                    else:
                        print("they dont match")
                else:
                    print("this key is not correct, please try again")
            elif username == "quit":
                print("process canceled")
                active = False
            else:
                print("this name doesnt exist")
        self.save_data(self.users)  #save new user data

    def initialize(self):
        """initializes the user. A username and password must be picked. A security key will be generated and all saved in the user.json file"""
        active = True
        while active:
            username = input("please enter your desired username\nprint 'quit' to exit the process\n>")
            if username in self.users.keys():
                print("the name already exists")

            elif username == "forgot" or username == "ini":
                print("this username is not available")

            elif username == "quit":
                active = False


            else:
                active = False
        active = True
        if username == "quit":
            pass
        else:
            while active:
                password = input("please enter your desired password\n>")
                reenter = input("please enter again\n>")
                if password == reenter:
                    self.users[username] = [password]
                    mkey = ""
                    for i in range(0, 10):
                        mkey += str(random.randint(0, 9))       #creates random security key as verification method for later password reset
                    self.users[username].append(mkey)
                    print("this is your security key:", mkey,
                          "keep it someplace safe\nyou are now able to login with these credentials\n")
                    active = False

                else:
                    print("they dont match")
            self.save_data(self.users)      #save user data

