# LF8 Projekt Team BitAir

## Getting Started 
Welcome dear visitor, we are proud to present you our software

Please download every library in order for the code to work. Note that all libraries work with "pip install library" as they appear 
in the code, except, MySQLdb, it can be installed via mysqlclient

Fell free to watch the Software Demo video to get a feeling for the functionalities of the program

## Functionalities
Now that you got everything you need please feel free and start the software:
- first you can initialize (if no user account was created)
- reset your password   (if an account exist)
- login to your account (if an account exist)


After logging in, you can:
- display all commands
- get a graph of mean values for each day   (add a system first)
- add a system to your account  (you need login data to our database: username: beer, password: 4uTDRsvP39, host: 31.17.80.101 )
- check your hardware status


## Future Plans
There are many things we could add in the future:
- implement a GUI
- more data queries and visualizations
- chatbot for general questions (maybe even ticket creation)
- upload and work with log files


