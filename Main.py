import sys
import time
import threading            #import import libraries
from Status import Status
from Data import Data   #import all classes
from User import User
from System import System


def check(a):
    """check the status of the workstation (CPU and RAM)"""
    status.check_status()       


def add_system(active_user):
    """adds a system to the user account"""
    system.get_credentials(active_user)
    data.get_data(active_user)


def dsmean(active_user):
    """displays the mean value of each day"""
    data.days_mean(active_user)


def get_help(active_user):
    """print all available commands"""
    for i, j in Commands.items():
        print(i + ": " + j[1])


Commands = {
    "help": [get_help, "list all commands"],
    "dsmean": [dsmean, "get a graph of mean values for each day"],
    "add": [add_system, "add a system to your account"],
    "check": [check, "check the CPU and RAM utilization"],
}
active = False      #had to initialze active for status class
user = User()
system = System(user)       #initialize all classes
data = Data(system)
status = Status(user, active)

if __name__ == "__main__":
    user.get_user_data()        #get user data, that was saved from previous sessions
    active_user = user.log_in()     #log in process and return username that will be the active_user
    data.get_data(active_user)  #get data from database, if a system was initialized by "add"
    x = threading.Thread(target=status.status_thread, args=(active_user,))      #initialize thread
    print("welcome to BitAir\n")
    active = True
    status.active = True   
    x.start()  #start threading
    while active:
        cmd = input("print 'help' to see all commands\n>").lower().split(" ")   #format input
        if cmd[0] in Commands:  #check if input is a proper command
            Commands[cmd[0]][0](active_user)    #execute command
        elif cmd[0] == "quit":  #exit program
            print("bitair shutting down ")
            status.active = False   #stop threading
            active = False
        else:
            print("this is not a valid command")
