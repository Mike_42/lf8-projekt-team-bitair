import MySQLdb
from User import User

class System:
    def __init__(self, user):
        self.credentials = {}
        self.system_user = user

    def get_credentials(self, active_user):
        """requests the credentials for the phpmyadmin page, checks whether they are valid or not, and saves these credentials in the user.json file"""
        username = input("please enter your username to log on to the system\ntpye 'quit' to leave the process\n>")
        password = input("please enter your password\n>")
        host_adress = input("please enter the host adress\n>")
        self.system_user.get_user_data()        #get user data for later usage
        cred = [host_adress, 3306, username, password]
        try:
            db = MySQLdb.connect(host=cred[0], port=cred[1], user=cred[2], passwd=cred[3], database='SensorValues')
            self.system_user.users[active_user].append(cred)    #here, it will be attached to the user data, retrieved before
            self.system_user.save_data(self.system_user.users)  #save the new user data
        except:
            print("something went wrong with the setup, please try again")

    def get_system(self, active_user):
        """returns all the data from the data base, given the credentials for the phpmyadmin page"""
        self.system_user.get_user_data()    #get user data
        try:        #if a system is linked to account it will return data
            u = self.system_user.users[active_user][2]  #read out all data regarding system credentials
            db = MySQLdb.connect(host=u[0], port=u[1], user=u[2], passwd=u[3], database='SensorValues') #connect do database
            cursor = db.cursor()
            cursor.execute("SELECT value1,reading_time FROM Sensor")    #execute query
            results = cursor.fetchall()     
            reading_data = [r for r in results] #save data
            return reading_data
        except IndexError:  #if no system is linked, it will return nothing
            return None
