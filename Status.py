import datetime as dt
import threading
import requests
import psutil       #import important libraries
import time
from User import User       #import class


class Status:
    def __init__(self, user, active):
        self.user = user
        self.active = active

    def check_status(self):
        """display RAM and CPU utilization"""
        ram = psutil.virtual_memory()[2]
        cpu = psutil.cpu_percent()
        if cpu >= 80 or ram >= 80:
            print("your system is very utilized:\nRAM: %d\nCPU: %d" % (ram, cpu))
        elif cpu >= 30 or ram >= 30:
            print("your system status is fine:\nRAM: %d\nCPU: %d" % (ram, cpu))
        elif cpu > 30 or ram > 30:
            print("your system is barely utilized:\nRAM: %d\nCPU: %d" % (ram, cpu))

    def save_log(self, active_user, ram, cpu):
        """save a log file with all important data for troubleshooting later"""
        self.user.get_user_data()       #get the user data to get tehe host adress to get a response from it
        try:    #if a system is linked to an account, this will be saved in a log
            host = self.user.users[active_user][2][0]
            response = requests.get("http://" + host)
            status_code = response.status_code
        except IndexError:  #if no system is linked, this will be saved in the log
            host = "no system linked to account"
            status_code = "none"
        now = str(dt.datetime.now()).replace(':', '-').replace(" ", "-")    
        save_now = now  #had to copy this datetime object, otherwise it will mess something up later on
        log_data = {"time stamp": now, "active user": active_user, "host adress": host, "URL status code": status_code, #notice this now...
                    "RAM": ram, "CPU": cpu, "system status": "very high utilization of resources, might cause problems"}
        log_info = ""
        for i, j in log_data.items():
            log_info += str(i) + ": " + str(j) + "\n"
        f = open(active_user + "-log" + save_now + ".txt", "w+")    #using 'now' instead of 'save_now' would result in different time stamps...
        f.write(log_info)   #save the log
        f.close()
        

    def status_thread(self, active_user):
        """background thread to monitor that the CPU and RAM are not utilized that much"""
        status_warning = []     
        while self.active:
            count = status_warning.count(True)  #counts the amount of True in status_warning
            ram = psutil.virtual_memory()[2]
            cpu = psutil.cpu_percent()
            if cpu >= 80 and ram >= 80:     #if the CPU and RAM are very utilized, it will append True to status_warning
                status_warning.append(True)
            else:       #if not, False will be returned
                status_warning.append(False)
            if count == 8:      #we want 8x True in status_warning, to look for consistent high utilization
                self.save_log(active_user, ram, cpu)        #save a log
                status_warning = []
            if len(status_warning) == 10:   #8/10 must be true and if 10 Booleans are stored, status_warning will reset
                status_warning = []
            time.sleep(2)
