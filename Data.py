import MySQLdb
import pandas as pd
import json         #import important libraries
import matplotlib.pyplot as plt
import datetime as dt
from User import User   #import class


class Data:
    def __init__(self, system):
        self.system = system
        self.readings = []

    def get_data(self, active_user):
        """get datbase data by executing get_systen"""
        self.readings = self.system.get_system(active_user)

    def save_plot(self, name):
        """either saves the plot as png or continues"""
        a = True
        while a:
            save = input("do you want the following data to be saved? ('y/n')\n>")
            if save == "y":
                now = str(dt.datetime.now()).replace(':', '-').replace(" ", "-")[:19]
                now = name + now + ".png"
                plt.savefig(now, format='png')      #save the plot with name of query and date
                print(now, "is safed in your directory")
                a = False
            elif save == "n":
                print("the file will not be saved")
                a = False
            else:
                print("please use 'y' or 'n'")

    def display_data(self, values, yv):
        """actually displays the data recieved in a plot"""
        name = "AverageScores-"
        ax = plt.axes()
        ax.set_ylim(-25, 120)
        ax.set_yticks([-25, 0, 25, 50, 75, 100])
        plt.ylabel("Humidity")
        plt.xlabel("Date")          #format all kinds of things on the plot
        plt.title("Humidity Measurement")
        plt.plot(values, "k", linewidth=1, marker="o")
        plt.plot(yv, [3 for i in range(len(values["Value"]))], "r:", linewidth=3)
        plt.legend(["Measure", "Warning"], loc=1)
        self.save_plot(name)       #save the plot
        plt.show()      #display the plot

    def days_mean(self, active_user):
        """loads the mean value for each day, given the data retrieved from the database"""
        if self.readings:   #if data was loaded into readings, then:
            data = pd.DataFrame(        #create dataframe because it is so much easier to work with any kind of data in a dataframe
                {"Value": [int(j[0]) for j in self.readings], "Date": [str(i[1])[2:10] for i in self.readings]})
            d_mean = data.groupby("Date").mean()    #group by the date and get the mean of each date
            self.display_data(d_mean, data["Date"].drop_duplicates())   #display the data
        else:
            print("there is no data available, use 'add' to link a system to your account")


