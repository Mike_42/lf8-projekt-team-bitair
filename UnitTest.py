import unittest
import json
from User import User
from System import System


class TestUser(unittest.TestCase):
    def setUp(self):
        self.user = User()
        self.system = System(self.user)
        self.test_dict = {"test": ["start123", "3243717145", ["31.17.80.101", 3306, "beer", "4uTDRsvP39"]]}

    def test_get_user_data(self):
        self.user.save_data(self.test_dict)
        self.user.get_user_data()
        self.assertEqual(self.test_dict["test"], self.user.users["test"])

    def test_log_in(self):
        print("use username=test and password=start123")
        self.user.get_user_data()
        self.assertEqual(self.user.log_in(), "test")

    def test_get_system(self):
        self.user.save_data(self.test_dict)
        data = self.system.get_system("test")
        self.assertEqual(bool(data), True)
